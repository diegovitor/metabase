job "loadbalance" {
  datacenters = ["dc1"]
  type        = "service"

  group "load-balancer" {
    count = 1

    network {

      port "http" {
        static = 80
      }

      port "https" {
        static = 443
      }

      port "dashboard" {
        static = 8080
      }
    }

    service {
      name = "lb"
      port = "http"
      provider= "consul"

      tags = [
            "traefik.enable=true",
            "traefik.http.routers.lb.rule=Host(`traefik.localhost`)",
            "traefik.http.routers.lb.service=api@internal",
            // "traefik.http.routers.lb.tls=true",
            "traefik.http.routers.lb.entrypoints=web,websecure,traefik",

      ]


      check {
        name     = "ready-tcp-http"
        type     = "tcp"
        port     = "http"
        interval = "3s"
        timeout  = "2s"
      }

      // check {
      //   name     = "ready-http"
      //   type     = "http"
      //   port     = "http"
      //   path     = "/"
      //   interval = "3s"
      //   timeout  = "2s"
      // }  

      check {
        name     = "ready-tcp-dashboard"
        type     = "tcp"
        port     = "dashboard"
        interval = "3s"
        timeout  = "2s"
      }

      check {
        name     = "ready-http-dashboard"
        type     = "http"
        port     = "dashboard"
        path     = "/"
        interval = "3s"
        timeout  = "2s"
      }
    }

    task "traefik" {
      driver = "docker"

      config {
        image        = "traefik:2.10"
        network_mode = "host"
        ports        = ["http", "https","dashboard"]
        args = [
                  "--api.dashboard=true",
                  "--api.insecure=true", ### For Test only, please do not use that in production
                  "--entrypoints.web.address=:${NOMAD_PORT_http}",
                  "--entrypoints.traefik.address=:${NOMAD_PORT_dashboard}",
                  "--entrypoints.websecure.address=:${NOMAD_PORT_https}",
                  // FIXME - Endereço do Consul
                  # Enables connect support, otherwise only http connections would be tried
                  # "--providers.consulcatalog.connectaware=true",
                  # Make the communication secure by default
                  #"--providers.consulcatalog.connectbydefault=true",
                  "--providers.consulcatalog=true",
                  "--providers.consulcatalog.exposedbydefault=false",
                  "--providers.consulcatalog.prefix=traefik",
                  "--providers.consulcatalog.endpoint.address=http://127.0.0.1:8500",
                  "--providers.consulcatalog.endpoint.scheme=http",
                  "--api=true",
                  "--log.level=DEBUG",
                  #"--log.filePath=/home/traefik.log",
                ]
      }
      
      resources {
        cpu    = 300
        memory = 50
      }
    }
  }
}
