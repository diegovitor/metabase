job "traefik" {
  // datacenters = ["dc1"]
  // type        = "service"
  #region      = "fortaleza"
  datacenters = ["dc1"]
  type        = "service"

  group "traefik" {
    count = 1

    network {
      mode = "host"

      port  "web"{
         static = 80
      }
      port  "admin"{
         static = 8080
      }
    }

    service {
      name = "lb"
      provider = "nomad"
      
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.lb.rule=Host(`traefik.localhost`)",
        "traefik.http.routers.lb.entryPoints=web",
        "traefik.http.routers.lb.tls=false",
        "traefik.http.services.lb.loadbalancer.server.port=8080"
      ]
    }

    task "server" {
      driver = "docker"
      config {
        image = "traefik:v2.9"
        ports = ["admin", "web"]
        args = [
          "--api.dashboard=true",
          "--api.insecure=true", ### For Test only, please do not use that in production
          "--entrypoints.web.address=:${NOMAD_PORT_web}",
          "--entrypoints.traefik.address=:${NOMAD_PORT_admin}",
          "--providers.nomad=true",
          "--providers.nomad.exposedbydefault=false",
          "--providers.nomad.endpoint.address=http://192.168.18.10:4646",
          "--api=true",
          "--log.level=DEBUG",
          #"--log.filePath=/home/traefik.log",
        ]
        network_mode = "host"
      }
    }
  }
}


# Adicionar endereço local dos serviços.
#   nomad agent -dev -region fortaleza -dc homologacao.dc.ufc.br
#   nomad agent -dev -bind 0.0.0.0 -log-level INFO
#   nomad run nomad_traefik.hcl 
# - echo "127.0.0.1       whoami1.com" >> /etc/hosts;
# - echo "127.0.0.1       whoami2.com" >> /etc/hosts;
