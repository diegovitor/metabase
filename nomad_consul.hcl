job "consul" {
  #region      = "fortaleza"
  datacenters = ["dc1"]
  type        = "service"

  group "consul" {
    count = 1

    network {
      port "consul-8500" {
        to = 8500
        static = 8500
      }

      port "consul-8600" {
        to = 8600
        static = 8600
      }       
    }

    service {
      name = "consul"
      provider = "nomad"
      port = "consul-8500"
    }

    task "consul-server" {
      driver = "docker"

      config {
        image = "consul:1.15.1"
        command = "consul"
        // FIXME - Expor somente para uma placa de rede
        args    = ["agent", "-dev", "-client=0.0.0.0", "-datacenter=dc1"]

        # Consul should always be run with --net=host in Docker because Consul's
        # consensus and gossip protocols are sensitive to delays and packet loss,
        # so the extra layers involved with other networking types are usually 
        # undesirable and unnecessary
        # https://hub.docker.com/_/consul
        network_mode = "host"
      }
    }
  }
}


# Adicionar endereço local dos serviços.
#   nomad agent -dev -bind 0.0.0.0 -log-level INFO
#   nomad agent -dev -region fortaleza -dc homologacao.dc.ufc.br 
#   nomad run nomad_traefik.hcl 
# - echo "127.0.0.1       whoami1.com" >> /etc/hosts;
# - echo "127.0.0.1       whoami2.com" >> /etc/hosts;

# garbage colector do nomad 

# nomad system gc