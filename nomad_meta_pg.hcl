variable "datacenters" {
  description = "Lista de Data Centers"
  type        = list(string)
  default     = ["dc1"]
}

variable "region" {
  description = "Região"
  type        = string
  default     = "global"
}

variable "postgres_db" {
  description = "Nome do Banco"
  default = "metabase"
}

variable "postgres_user" {
  description = "Usuário Postgres"
  default = "postgres"
}

variable "postgres_password" {
  description = "Postgres DB Password"
  default = "password"
}

variable "meta_port" {
  description = "Metabase App"
  default = 3000
}

job "postgres" {
  datacenters = var.datacenters
  region = var.region
  type = "service"

 group "db" {
    count = 1
    network {
      port "db" {
        static = 5432
      }
    }

    task "db" {
      lifecycle {
        hook = "prestart"
        sidecar = false
      }
      driver = "docker"
      service {
        name = "database"
        provider = "nomad"
        port = "db"
      }

      meta {
        service = "postgres"
      }

      config {
        image   = "postgres"
        ports = ["db"]
        network_mode = "host"      
      }
      env {
        POSTGRES_DB       = var.postgres_db
        POSTGRES_USER     = var.postgres_user
        POSTGRES_PASSWORD = var.postgres_password
      }
    }
  }
}