variable "datacenters" {
  description = "Lista de Data Centers"
  type        = list(string)
  default     = ["dc1"]
}

variable "region" {
  description = "Região"
  type        = string
  default     = "global"
}

variable "postgres_db" {
  description = "Nome do Banco"
  default     = "metabase"
}

variable "postgres_user" {
  description = "Usuário Postgres"
  default     = "postgres"
}

variable "postgres_password" {
  description = "Password do Database"
  default     = "password"
}

variable "meta_port" {
  description = "Metabase App"
  default     = 3000
}

job "metabase" {

  datacenters = var.datacenters
  region      = var.region
  type        = "service"

  group "metagroup" {
    count = 1

    network {
      port "admin" {
        static = var.meta_port
      }
    }

    meta {
      service = "metabase"
    }

    service {
      name     = "metabase"
      tags     = ["metabase"]
      port     = "admin"
      provider = "nomad"
    }

    task "metabase" {
      driver = "docker"
      config {
        image        = "metabase/metabase:latest"
        network_mode = "host"
        ports        = ["admin"]
      }

      env {
        MB_DB_TYPE   = "postgres"
        MB_DB_DBNAME = var.postgres_db
        MB_DB_PORT   = "5432"
        MB_DB_USER   = var.postgres_user
        MB_DB_PASS   = var.postgres_password
        MB_DB_HOST   = "127.0.0.1"
      }

      resources {
        cpu    = 500
        memory = 1024
      }
    }
  }
}